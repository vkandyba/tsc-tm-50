package ru.vkandyba.tm.service;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;

import javax.jms.*;

public class ReceiverService {

    private final ConnectionFactory connectionFactory;

    public ReceiverService(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @SneakyThrows
    public void receive(final MessageListener listener){
        final Connection connection = connectionFactory.createConnection();
        connection.start();
        final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        final Destination destination = session.createTopic("TM");
        final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(listener);
    }
}
