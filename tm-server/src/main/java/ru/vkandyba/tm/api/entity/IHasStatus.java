package ru.vkandyba.tm.api.entity;

import ru.vkandyba.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
