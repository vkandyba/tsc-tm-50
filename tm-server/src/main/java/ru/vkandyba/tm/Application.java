package ru.vkandyba.tm;

import ru.vkandyba.tm.component.Bootstrap;
import ru.vkandyba.tm.service.PropertyService;
import ru.vkandyba.tm.util.HashUtil;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}

