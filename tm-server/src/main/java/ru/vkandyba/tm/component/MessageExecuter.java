package ru.vkandyba.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.dto.EntityLogDTO;
import ru.vkandyba.tm.listener.EntityListener;
import ru.vkandyba.tm.service.SenderService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MessageExecuter {

    private static final int THREAD_COUNT = 3;

    @NotNull
    private final SenderService service = new SenderService();

    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

    public void sendMessage(@NotNull final Object object, @NotNull final String type) {
        es.submit( () -> {
            @NotNull final EntityLogDTO entity = service.createMessage(object, type);
            service.send(entity);
        });
    }
}
