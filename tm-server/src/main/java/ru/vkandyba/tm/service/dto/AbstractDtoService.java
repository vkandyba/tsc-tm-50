package ru.vkandyba.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.api.service.IConnectionService;

public abstract class AbstractDtoService {

    @NotNull
    public final IConnectionService connectionService;

    public AbstractDtoService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
