package ru.vkandyba.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.dto.ProjectDTO;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.exception.empty.EmptyNameException;
import ru.vkandyba.tm.repository.dto.AbstractDtoRepository;
import ru.vkandyba.tm.repository.dto.ProjectDtoRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectDtoService extends AbstractDtoService {

    public ProjectDtoService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    public List<ProjectDTO> findAll(String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            return projectRepository.findAll(userId);
        } catch (Exception e){
            throw e;
        } finally {
            entityManager.close();
        }
    }

    
    @Nullable
    public ProjectDTO findByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            return projectRepository.findByName(userId, name);
        } catch (Exception e){
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public ProjectDTO findById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            return projectRepository.findByName(userId, id);
        } catch (Exception e){
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void add(String userId, ProjectDTO project) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            project.setUserId(userId);
            projectRepository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    
    public void removeByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            projectRepository.removeByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void removeById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            projectRepository.removeByName(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    
    public void updateById(String userId, String id, String name, String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if(name == null || name.isEmpty()) throw new EmptyNameException();
        if(description == null || description.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            projectRepository.updateById(userId, id, name, description);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    
    public void startById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            projectRepository.startById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    
    public void startByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            projectRepository.startByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    
    public void finishById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            projectRepository.finishById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    
    public void finishByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            projectRepository.finishByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void clear(String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
