package ru.vkandyba.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.component.Bootstrap;
import ru.vkandyba.tm.marker.SoapCategory;

import java.util.List;

public class TaskEndpointTest {

//    @NotNull
//    Session session = new Session();
//
//    @NotNull
//    ServiceLocator serviceLocator = new Bootstrap();
//
//    @NotNull
//    String login = "test";
//
//    @NotNull
//    String password = "test";
//
//    @NotNull
//    String TaskId = "123456790";
//
//    @NotNull
//    String TaskName = "test";
//
//    @NotNull
//    Integer TaskIndex = 0;
//
//    @NotNull
//    String userId = "1";
//
//    @NotNull
//    Status status = Status.COMPLETED;
//
//    @Before
//    public void before(){
//        session = serviceLocator.getSessionEndpoint().openSession(login, password);
//        final Task Task = new Task();
//        Task.setId(TaskId);
//        Task.setUserId(userId);
//        Task.setName(TaskName);
//        serviceLocator.getTaskEndpoint().addTask(session, Task);
//    }
//
//    @After
//    public void after(){
//        serviceLocator.getTaskEndpoint().clearTasks(session);
//        serviceLocator.getSessionEndpoint().closeSession(session);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void findAll() {
//        List<Task> Tasks = serviceLocator.getTaskEndpoint().findAllTasks(session);
//        Assert.assertEquals(1, Tasks.size());
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void startById() {
//        final Task Task = serviceLocator.getTaskEndpoint().startTaskById(session, TaskId);
//        Assert.assertNotNull(Task);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void startByIndex() {
//        final Task Task = serviceLocator.getTaskEndpoint().startTaskByIndex(session, TaskIndex);
//        Assert.assertNotNull(Task);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void startByName() {
//        final Task Task = serviceLocator.getTaskEndpoint().startTaskByName(session, TaskName);
//        Assert.assertNotNull(Task);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void finishById() {
//        final Task Task = serviceLocator.getTaskEndpoint().finishTaskById(session, TaskId);
//        Assert.assertNotNull(Task);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void finishByIndex() {
//        final Task Task = serviceLocator.getTaskEndpoint().finishTaskByIndex(session, TaskIndex);
//        Assert.assertNotNull(Task);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void finishByName() {
//        final Task Task = serviceLocator.getTaskEndpoint().finishTaskByName(session, TaskName);
//        Assert.assertNotNull(Task);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void changeStatusById() {
//        final Task Task = serviceLocator.getTaskEndpoint().changeTaskStatusById(session, TaskId, status);
//        Assert.assertNotNull(Task);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void changeStatusByIndex() {
//        final Task Task = serviceLocator.getTaskEndpoint().changeTaskStatusByIndex(session, TaskIndex, status);
//        Assert.assertNotNull(Task);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void changeStatusByName() {
//        final Task Task = serviceLocator.getTaskEndpoint().changeTaskStatusByName(session, TaskName, status);
//        Assert.assertNotNull(Task);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void removeById() {
//        final Task Task = serviceLocator.getTaskEndpoint().removeTaskById(session, TaskId);
//        Assert.assertNotNull(Task);
//    }
//
//    @Test
//    @Category(SoapCategory.class)
//    public void removeByIndex() {
//        final Task Task = serviceLocator.getTaskEndpoint().removeTaskByIndex(session, TaskIndex);
//        Assert.assertNotNull(Task);
//    }

}
